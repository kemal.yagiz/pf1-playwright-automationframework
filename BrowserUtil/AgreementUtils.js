let agreementNumber;
let accessToken;

function getAgreementNumber() {
    return agreementNumber;
}

function setAgreementNumber(number) {
    agreementNumber = number;
}

function getAccessToken() {
    return accessToken;
}

function setAccessToken(token) {
    accessToken = token;
}

module.exports = { getAgreementNumber, setAgreementNumber, getAccessToken, setAccessToken };
