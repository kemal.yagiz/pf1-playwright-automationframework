let accessToken;

function getAccessToken() {
    return accessToken;
}

function setAccessToken(token) {
    accessToken = token;
}

module.exports = { getAccessToken, setAccessToken };