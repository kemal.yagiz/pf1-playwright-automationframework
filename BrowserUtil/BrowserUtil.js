const axios = require('axios');
const { getAccessToken } = require('../BrowserUtil/AccessTokenHandler');
const { generateNewAgreementNumber } = require('../BrowserUtil/AgreementNumberGenerator');
const { setAgreementNumber } = require('../BrowserUtil/AgreementUtils');
const { getAgreementNumber } = require('../BrowserUtil/AgreementUtils');

// Define the BrowserUtil class
class BrowserUtil {
    
  constructor(page) {
    this.page = page;
  }
  
  async login() {
    await this.page.goto("https://he-int1-bt-pvtweb.lenvi.com/fo/home");
    
    // Fill email
    await this.page.locator("//div[contains(@class, 'placeholderContainer')]//input[@type='email' and @id='i0116']")
      .fill("kemal.yagiz@lenvi.com");
    await this.page.click('input#idSIButton9');
    
    // Fill password
    await this.page.locator("//input[@type='password' and @id='i0118']")
      .fill("41504150aaA.");
    await this.page.click('input[type="submit"][value="Sign in"]');
    
    // Click "Yes"
    await this.page.click('input[type="submit"][value="Yes"]');
  }
  async loginSIT1() {
    await this.page.goto("https://he-sit1-et-pvtweb.lenvi.com/fo/home");
    
    // Fill email
    await this.page.locator("//div[contains(@class, 'placeholderContainer')]//input[@type='email' and @id='i0116']")
      .fill("kemal.yagiz@lenvi.com");
    await this.page.click('input#idSIButton9');
    
    // Fill password
    await this.page.locator("//input[@type='password' and @id='i0118']")
      .fill("41504150aaA.");
    await this.page.click('input[type="submit"][value="Sign in"]');
    
    // Click "Yes"
    await this.page.click('input[type="submit"][value="Yes"]');
  }

  async getAccessTokenSIT1() {
    try {
        const client_id = "4c62c689-e6f6-46fe-a98d-d31c8c26256d";
        const client_secret = "vnQ8Q~twhR4qtzSoLe3rm2bR99fmQBq9VZGHQdll";
        const scope = "api://684c9cc0-20b2-48b6-a279-cf5ba1cf1636/.default";
        const grant_type = "client_credentials";

        const requestBody = `grant_type=${grant_type}&` +
            `client_id=${client_id}&` +
            `client_secret=${client_secret}&` +
            `scope=${scope}`;

        const tokenEndpoint = "https://login.microsoftonline.com/0de6761e-b4b2-4eb3-9b4a-a440d224f7e5/oauth2/v2.0/token";

        const response = await axios.post(tokenEndpoint, requestBody, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        const accessToken = response.data.access_token;
        console.log("Access Token:", accessToken);
        
        // Set the access token to use it later
        this.accessToken = accessToken;

        return accessToken;
    } catch (error) {
        console.error('Error getting access token:', error.message);
        throw error;
    }
}


  async getAccessToken() {
    try {
        const client_id = "7dadb46d-0731-4d81-9594-c327812c28fe";
        const client_secret = "2OT8Q~haZpVP3YdjE45O.m-isvTTWhEhavsNedsA";
        const scope = "api://bc4b5df1-6140-407e-9b6e-ff950f936c7f/.default";
        const grant_type = "client_credentials";

        const requestBody = `grant_type=${grant_type}&` +
            `client_id=${client_id}&` +
            `client_secret=${client_secret}&` +
            `scope=${scope}`;

        const tokenEndpoint = "https://login.microsoftonline.com/0de6761e-b4b2-4eb3-9b4a-a440d224f7e5/oauth2/v2.0/token";

        const response = await axios.post(tokenEndpoint, requestBody, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        const accessToken = response.data.access_token;
        console.log("Access Token:", accessToken);
        
        // Set the access token to use it later
        this.accessToken = accessToken;

        return accessToken;
    } catch (error) {
        console.error('Error getting access token:', error.message);
        throw error;
    }
  }

  async createAgreementPF1INT1() {
    try {
        const accessToken = await this.getAccessToken();

        // JSON payload for creating the agreement
        const s = generateNewAgreementNumber();
        const agreementPayload = {
            agreementNumber: s,
            solicitorCode: 'tests',
            brandId: 10098,
            equityLoanPercentage: 20,
            agreementComponent: {
                completionDate: '2021-08-23',
                advance: 40000,
                paymentDay: 1
            },
            brokerList: [
                {
                    brokerType: 'Main',
                    broker: {
                        code: 'testb'
                    }
                }
            ],
            applicantList: [
                {
                    applicantType: 'Main',
                    person: {
                        title: 'Mr',
                        forename: 'Kemal',
                        middleName: '',
                        surname: 'Tester',
                        dateOfBirth: '1993-01-01',
                        homePhoneNumber: '01252998877',
                        mobilePhoneNumber: '079879988771',
                        emailAddress: 'kemal.yagiz@lenvi.com',
                        addressHistoryList: [
                            {
                                address: {
                                    flat: null,
                                    propertyNumber: '2',
                                    propertyName: '',
                                    street: 'High St',
                                    district: 'Clifton',
                                    town: 'York',
                                    postCode: 'YO99 9ZZ',
                                    county: 'N Yorks',
                                    country: 'GB'
                                },
                                movingInDate: '2000-04-20'
                            }
                        ],
                        preferredContactMethod: 'EMAIL'
                    }
                }
            ],
            security: {
                tenure: 'F',
                address: {
                    propertyNumber: '1',
                    propertyName: '',
                    street: 'High St',
                    district: 'Clifton',
                    town: 'York',
                    postCode: 'YO99 9ZZ',
                    country: 'GB'
                },
                buildDate: '2001-07-02',
                purchasePrice: 200000,
                purchaseDate: '2020-07-02',
                firstMortgageAmount: 160000,
                firstMortgageTerm: 23,
                firstMortgageLender: 'The original mortgage lender',
                deposit: 40000
            },
            bankAccount: {
                sortCode: '001122',
                accountNumber: '12345678',
                bankName: 'HSBC',
                branchName: 'York'
            }
        };

        // URL for agreement creation endpoint
        const agreementCreationURL = 'https://he-int1-bt-pvtweb.lenvi.com/borrowbox-api/api/agreement';

        // Make POST request to create agreement
        const response = await axios.post(agreementCreationURL, agreementPayload, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        // Print response
        console.log('Agreement creation response:', response.data);
        setAgreementNumber(s);
    } catch (error) {
        console.error('Error creating agreement:', error.message);
    }
  }
  async createAgreementPF1SIT1() {
    try {
        const accessToken = await this.getAccessTokenSIT1();

        // JSON payload for creating the agreement
        const s = generateNewAgreementNumber();
        const agreementPayload = {
            agreementNumber: s,
            solicitorCode: 'tests',
            brandId: 10098,
            equityLoanPercentage: 20,
            agreementComponent: {
                completionDate: '2021-08-23',
                advance: 40000,
                paymentDay: 1
            },
            brokerList: [
                {
                    brokerType: 'Main',
                    broker: {
                        code: 'testb'
                    }
                }
            ],
            applicantList: [
                {
                    applicantType: 'Main',
                    person: {
                        title: 'Mr',
                        forename: 'Kemal',
                        middleName: '',
                        surname: 'Tester',
                        dateOfBirth: '1993-01-01',
                        homePhoneNumber: '01252998877',
                        mobilePhoneNumber: '079879988771',
                        emailAddress: 'kemal.yagiz@lenvii.com',
                        addressHistoryList: [
                            {
                                address: {
                                    flat: null,
                                    propertyNumber: '2',
                                    propertyName: '',
                                    street: 'High St',
                                    district: 'Clifton',
                                    town: 'York',
                                    postCode: 'YO99 9ZZ',
                                    county: 'N Yorks',
                                    country: 'GB'
                                },
                                movingInDate: '2000-04-20'
                            }
                        ],
                        preferredContactMethod: 'EMAIL'
                    }
                }
            ],
            security: {
                tenure: 'F',
                address: {
                    propertyNumber: '1',
                    propertyName: '',
                    street: 'High St',
                    district: 'Clifton',
                    town: 'York',
                    postCode: 'YO99 9ZZ',
                    country: 'GB'
                },
                buildDate: '2001-07-02',
                purchasePrice: 200000,
                purchaseDate: '2020-07-02',
                firstMortgageAmount: 160000,
                firstMortgageTerm: 23,
                firstMortgageLender: 'The original mortgage lender',
                deposit: 40000
            },
            bankAccount: {
                sortCode: '001122',
                accountNumber: '12345678',
                bankName: 'HSBC',
                branchName: 'York'
            }
        };

        // URL for agreement creation endpoint
        const agreementCreationURL = 'https://he-sit1-et-pvtweb.lenvi.com/borrowbox-api/api/agreement';

        // Make POST request to create agreement
        const response = await axios.post(agreementCreationURL, agreementPayload, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        // Print response
        console.log('Agreement creation response:', response.data);
        setAgreementNumber(s);
    } catch (error) {
        console.error('Error creating agreement:', error.message);
    }
}

  async activateAgreementPF1INT1() {
    try {
        // Retrieve access token from the class instance
        const accessToken = this.accessToken;
        
        // Retrieve agreement number from wherever it is stored
        const agreementNumber = getAgreementNumber(); // Assuming this function exists to retrieve the agreement number

        // Construct endpoint URL
        const endpoint = `https://he-int1-bt-pvtweb.lenvi.com/borrowbox-api/api/agreement/${agreementNumber}/activate`;
        console.log("Agreement number:", agreementNumber);
        console.log("AccessToken:", accessToken);

        // Send POST request to activate agreement
        const response = await axios.post(endpoint, {}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        // Print response
        console.log("Agreement activation response:", response.data);
        // Assert response status code if needed
        if (response.status === 200) {
            console.log("Agreement activated successfully.");
        } else {
            console.error("Agreement activation failed with status code:", response.status);
        }
    } catch (error) {
        console.error('Error activating agreement:', error.message);
    }
    
    
}
async activateAgreementPF1SIT1() {
    try {
        // Retrieve access token from the class instance
        const accessToken = this.accessToken;
        
        // Retrieve agreement number from wherever it is stored
        const agreementNumber = getAgreementNumber(); // Assuming this function exists to retrieve the agreement number

        // Construct endpoint URL
        const endpoint = `https://he-sit1-et-pvtweb.lenvi.com/borrowbox-api/api/agreement/${agreementNumber}/activate`;
        console.log("Agreement number:", agreementNumber);
        console.log("AccessToken:", accessToken);

        // Send POST request to activate agreement
        const response = await axios.post(endpoint, {}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            }
        });

        // Print response
        console.log("Agreement activation response:", response.data);
        // Assert response status code if needed
        if (response.status === 200) {
            console.log("Agreement activated successfully.");
        } else {
            console.error("Agreement activation failed with status code:", response.status);
        }
    } catch (error) {
        console.error('Error activating agreement:', error.message);
    }
}

async runTest() {
    try {
        await this.getAccessToken();
        await this.createAgreementPF1INT1();
        await this.activateAgreementPF1INT1();
        await this.login();
    } catch (error) {
        console.error('Error running test:', error.message);
    }
}
async runTestSIT1() {
    try {
        await this.getAccessTokenSIT1();
        await this.createAgreementPF1SIT1();
        await this.activateAgreementPF1SIT1();
        await this.loginSIT1();
    } catch (error) {
        console.error('Error running test:', error.message);
    }
}





}

module.exports = BrowserUtil; // Exporting the BrowserUtil class
