const axios = require('axios');
const { setAgreementNumber } = require('./AgreementUtils'); 

function generateRandomString() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01123456789';
    const minLength = 5;
    const maxLength = 10;
    const length = Math.floor(Math.random() * (maxLength - minLength + 1)) + minLength;

    let randomString = '';
    for (let i = 0; i < length; i++) {
        randomString += characters.charAt(Math.floor(Math.random() * characters.length));
    }

    return randomString;
}

async function createAgreement() {
    try {
        const numberOfDependants = 1;
        const dependantsAges = "35";

        const randomSurname = generateRandomString();

        const agreementPayload = {
            campaignCode: "BROKER",
            brokerReference: "BK089",
            loanPurpose: "Home Improvements",
            advance: 5000,
            term: 18,
            consentToProceed: true,
            applicantList: [
                {
                    applicantType: "Main",
                    person: {
                        title: "Mr",
                        forename: "Kemal",
                        middleName: null,
                        surname: randomSurname,
                        dateOfBirth: "1980-01-01",
                        homePhoneNumber: "0123456789",
                        mobilePhoneNumber: "07123456789",
                        emailAddress: "test@example.com",
                        maritalStatus: "Single",
                        numberOfDependants: numberOfDependants,
                        dependantsAges: dependantsAges,
                        netMonthlyIncome: "3000",
                        bankAccountAcceptsDD: true,
                        underDebtManagement: false,
                        addressHistoryList: [
                            {
                                address: {
                                    propertyNumber: "1",
                                    propertyName: "A1",
                                    street: "High Street",
                                    district: "",
                                    town: "Anytown",
                                    postCode: "CB6 1AS"
                                },
                                movingInDate: "2010-01-01"
                            }
                        ],
                        employment: {
                            status: "Employed",
                            selfEmploymentHistory: null,
                            employmentHistoryList: [
                                {
                                    employerName: "Company Inc.",
                                    headOfficeNumber: null,
                                    workNumber: "01234567890",
                                    occupation: "Developer",
                                    position: "Private Sector",
                                    startDate: "2010-01-01",
                                    address: {
                                        propertyNumber: "1",
                                        propertyName: null,
                                        street: "Main Road",
                                        district: null,
                                        town: "London",
                                        postCode: "E1 1EE"
                                    }
                                }
                            ]
                        },
                        bankAccount: {
                            bankName: "Halifax",
                            sortCode: "111486",
                            accountName: "John Doe",
                            accountNumber: "10000002",
                            timeAtBankYears: 10,
                            timeAtBankMonths: 5
                        },
                        affordabilityList: [],
                        residence: {
                            propertyOwner: false,
                            residentialStatus: "Tenant",
                            tenancyType: "Private"
                        },
                        consent: {
                            creditSearch: true,
                            mailshots: false,
                            telephone: false,
                            mobile: false,
                            email: false,
                            forwardDetailsToThirdParty: false,
                            openBanking: false
                        }
                    }
                }
            ],
            security: null,
            additionalAttributes: null
        };

        const agreementCreationURL = 'https://ell-int1-bt-pvtweb.lenvi.com/ell-new-business-api/api/brokers/BK224/applications/full';

        const response = await axios.post(agreementCreationURL, agreementPayload, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic QksyMjQ6VDNTVEJLMjI0'
            }
        });

        console.log('Agreement creation response:', response.data);
        
       
        const agreementNumber = response.data.agreementNumber;
        setAgreementNumber(agreementNumber);

    } catch (error) {
        console.error('Error creating agreement:', error.response ? error.response.data : error.message);
    }
}

module.exports = { createAgreement };