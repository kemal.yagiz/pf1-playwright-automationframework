function generateRandomFirstName() {
    const firstNames = ["John", "Jane", "Alice", "Bob", "Michael", "Emily", "Chris", "Jessica"];
    return firstNames[Math.floor(Math.random() * firstNames.length)];
}
function generateRandomLastName() {
    const lastNames = ["Doe", "Smith", "Johnson", "Williams", "Brown", "Davis", "Miller", "Wilson"];
    return lastNames[Math.floor(Math.random() * lastNames.length)];
}
