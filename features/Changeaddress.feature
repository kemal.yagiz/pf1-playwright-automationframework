Feature: Change address

  # HEPF1-T19
  Scenario: Change address details
    Given I am logged in to the application T19
    When I click on the agreement search button T19
    And I fill in the agreement number T19
    And I click on the search button T19
    And I click on the first agreement from the search results T19
    And I wait for 1 second T19
    And I click on the IDV button T19
    And I click on the View Agreement button T19
    And I click on Actions T19
    And I select Address History from the actions menu T19
    And I click on Edit for address details T19
    And I update property number to 2 T19
    And I update property name to kemal T19
    And I update street to High Lenvi Street T19
    And I update town to Leeds T19
    And I update postcode to LE24NY T19
    And I select start date as DD-MM-YYYY T19
    And I submit the address details T19
    Then I should see the updated street details High Lenvi Street T19
    And I close the page T19
