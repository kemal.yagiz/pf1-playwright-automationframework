Feature: Direct Debit Next Payment Amount T39

  # HEPF1-T39
  Scenario: Direct debit next payment amount verification T39
    Given I am logged in to the application T39
    When I click on the agreement search button T39
    And I fill in the agreement number T39
    And I click on the search button T39
    And I click on the first agreement from the search results T39
    And I wait for 1 second T39
    And I click on the IDV button T39
    And I click on the View Agreement button T39
    Then I should verify the next payment amount is £0.00 T39
    And I should see the Next Payment title T39
