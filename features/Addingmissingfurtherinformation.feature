Feature: Adding missing further information
#T16
  Scenario: Add missing further information
    Given I am logged in to the application T16
    When I click on the agreement search button T16
    And I fill in the agreement number T16
    And I click on the search button T16
    And I click on the first agreement from the search results T16
    And I wait for 1 second T16
    And I click on the IDV button T16
    And I click on the View Agreement button T16
    And I click on Actions T16
    And I select Situation from the actions menu T16
    And I wait for 1.5 seconds T16
    And I click on Not Vulnerable T16
    And I wait for 2 seconds T16
    And I click on Vulnerable T16
    And I wait for 2 seconds T16
    And I click on the resilience input field T16
    And I select Inadequate or Erratic Income from the dropdown T16
    And I click on the health input field T16
    And I select Addiction from the dropdown T16
    And I click on the life event input field T16
    And I select Bereavement from the dropdown T16
    And I click on the capability input field T16
    And I select Learning Difficulties from the dropdown T16
    And I click on the consent to hold checkbox T16
    And I click on the review date input field T16
    And I select the date T16
    And I click on the save button T16
    Then I should see an error message stating Missing further information T16
    And I close the page T16
