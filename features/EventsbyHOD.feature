Feature: Events by HOD T42 T41 T30 T22

  Scenario: Verify scripts
    Given I am logged in to the application T42 T41 T30 T22
    When I search for the agreement T42 T41 T30 T22
    And I select the agreement from the search results T42 T41 T30 T22
    And I view the agreement details T42 T41 T30 T22
    And I navigate to the Scripts section T42 T41 T30 T22
    And I verify the Direct Debit Script T42 T41 T30 T22
    And I verify the Outbound Call Script T42 T41 T30 T22
    And I verify the Outbound Voicemail Script T42 T41 T30 T22
    Then I close the browser T42 T41 T30 T22
