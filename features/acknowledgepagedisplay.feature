Feature: Acknowledge Page Display
#HEPF1-T15
Scenario: Acknowledge Page should be displayed
    Given I am logged in
    When I search for an agreement with agreement number
    And I select the agreement from the search results
    And I click on the IDV button
    And I click on the View Agreement button
    Then I should see a message indicating that acknowledgment is required for the agreement
    And I acknowledge the agreement
    And I hover over the vulnerable tag
    Then I should see a message indicating vulnerability details
