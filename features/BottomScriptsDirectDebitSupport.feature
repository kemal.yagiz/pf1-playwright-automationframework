Feature: Bottom Scripts, Direct Debit Support, Outbound Call, Outbound Voicemail

  # HEPF1-T32
  Scenario: Verifying bottom scripts, direct debit support, outbound call, and outbound voicemail T32
    Given I am logged in to the application T32
    When I click on the agreement search button T32
    And I wait for 2 seconds T32
    And I fill in the agreement number T32
    And I click on the search button T32
    And I click on the first agreement from the search results T32
    And I wait for 1 second T32
    And I click on the IDV button T32
    And I click on the View Agreement button T32
    And I click on the Events button T32
    And I wait for 1.5 second T32
    Then I should see PF1 Access T32
    And I should see New Property Valuation T32
    And I should see Valuation Type: Purchase Price, Amount: 200000 T32
    And I should see New Bank Account: 001122 12345678 T32
