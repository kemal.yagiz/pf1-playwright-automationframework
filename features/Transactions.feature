Feature: Transactions T34

  Scenario: Perform a payment transaction T34
    Given I am logged in to the application T34
    When I search for the agreement T34
    And I select the agreement from the search results T34
    And I navigate to view agreement details T34
    And I take a payment T34
    Then I verify the payment transaction T34
    And I close the browser T34
