Feature: HEPF1-T23

  Scenario: HOD change contact details
    Given I am logged in to the application T23
    When I click on the agreement search button T23
    And I fill in the agreement number T23
    And I click on the search button T23
    And I click on the first agreement from the search results T23
    And I wait for 1 second T23
    And I click on the IDV button T23
    And I click on the View Agreement button T23
    And I click on Actions T23
    And I select Contact Details from the menu T23
    And I fill in the home phone number
    And I fill in the mobile phone number
    And I fill in the email address
    And I click on the Save button
    Then I verify the updated phone number and email address
    
