Feature: Notes

  Scenario: Verify and update vulnerability notes
    Given I am logged in to the application T31
    When I search for the agreement T31
    And I select the agreement from the search results T31
    And I view the agreement details T31
    And I navigate to the situation section T31
    And I update the vulnerability status T31
    And I verify the updated vulnerability notes T31
    And I close the browser T31
