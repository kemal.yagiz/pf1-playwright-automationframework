Feature: Property Details Tab

  Scenario: View property details t28
    Given I am logged in to the application t28
    When I search for the agreement t28
    And I select the agreement from the search results t28
    And I navigate to view property details t28
    Then I verify the property address and amount t28
    And I close the browser t28
