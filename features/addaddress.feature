Feature: Add Address
#HEPF1-T17
  Scenario: Add address
    Given I am logged in Address
    When I search for an agreement with agreement number X7GWAG1 Address
    And I click on the agreement from the search results Address
    And I click on the IDV button Address
    And I click on the View Agreement button Address
    And I click on Actions Address
    And I select Address History Address
    And I click on Add Address Address
    And I fill in the address details Address
    And I submit the address form Address
    Then I should see the newly added address in the address history Address
    And I remove the second address from the history Address
