Feature: Loan Details Schedules

  Scenario: Verify loan repayment schedules
    Given I am logged in to the application T27
    When I search for the agreement T27
    And I select the agreement from the search results T27
    And I view the agreement details T27
    And I navigate to the schedules section T27
    Then I verify the repayment details T27
    And I verify the repayment amount T27
    And I take a screenshot T27
    And I close the browser T27
