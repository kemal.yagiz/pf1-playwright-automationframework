Feature: Open Cases Tab

  Scenario: Verify Open Cases Tab and View Button
    Given I am logged in to the application HEPF1-T29
    When I search for the agreement HEPF1-T29
    And I select the agreement from the search results HEPF1-T29
    And I navigate to the agreement details HEPF1-T29
    Then I verify the Open Cases title HEPF1-T29
    And I verify the visibility of the View button HEPF1-T29
    And I close the browser HEPF1-T29
