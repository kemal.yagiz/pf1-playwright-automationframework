Feature: Change Contact Preferences T24

  # HEPF1-T24
  Scenario: Changing contact preferences T24
    Given I am logged in to the application T24
    When I click on the agreement search button T24
    And I fill in the agreement number T24
    And I click on the search button T24
    And I click on the first agreement from the search results T24
    And I wait for 1 second T24
    And I click on the IDV button T24
    And I click on the View Agreement button T24
    And I click on Actions T24
    And I select Contact Details from the actions menu T24
    And I click on Contact at Home T24
    And I click on Contact on Mobile T24
    And I click on Contact at Work T24
    And I fill in the mobile phone number with 07987998877 T24
    And I click on Save T24
    
