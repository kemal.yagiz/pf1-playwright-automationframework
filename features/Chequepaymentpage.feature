#T85
Feature:  User should be able to see take payment tab
Scenario: User sees cheque tab
Given ​User navigates to the front office t85
Given User selects an existing agreement t85
Given User accesses the Financials tab t85
Given User sees cheque payment and Take payment t85
Given User clicks cheque payment and enters amount t85
Given User should see the success message t85