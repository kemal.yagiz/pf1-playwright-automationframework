Feature: Update Personal Details by HOD

  Scenario: Update personal details t26 t8
    Given I am logged in to the application t26 t8
    When I search for the agreement t26 t8
    And I select the agreement from the search results t26 t8
    And I navigate to Personal Details section t26 t8
    And I update personal details with new information t26 t8
    Then I verify the updated name t26 t8
    And I close the browser t26 t8
