Feature: Financials Tab Card Payment

  Scenario: Perform card payment on financials tab T35
    Given I am logged in to the application T35
    When I click on the agreement search button T35
    And I fill in the agreement number T35
    And I click on the search button T35
    And I click on the first agreement from the search results T35
    And I wait for 1 second T35
    And I click on the IDV button T35
    And I click on the View Agreement button T35
    And I click on the Take Payment button T35
    And I choose Cash from the payment dropdown T35
    And I fill in the payment amount as 20 T35
    And I fill in the comment as TEST KEMAL T35
    And I fill in the reference as Lenvi Kemal T35
    Then I close the browser T35
