#T82
Feature:  User should be able to see card payment tab
Scenario: User sees card payment tab
Given ​User navigates to the front office
Given User selects an existing agreement
Given User accesses the Financials tab
Given User sees Card payment and Take payment
Given User clicks Card payment and enters amount
Given User should be on the card details tab