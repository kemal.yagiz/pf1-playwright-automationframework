Feature: HEPF1-T25

  Scenario: HOD add additional contact details
    Given I am logged in to the application T25
    When I click on the agreement search button T25
    And I fill in the agreement number T25
    And I click on the search button T25
    And I click on the first agreement from the search results T25
    And I wait for 1 second T25
    And I click on the IDV button T25
    And I click on the View Agreement button T25
    And I click on Actions
    And I select Contact Details from the menu
    And I click on Add button
    And I fill in the phone number
    And I select Relative from the contact type dropdown
    And I fill in the description as RE
    And I click on the second Add button
    And I save the details
    Then I close the browser T25
