const { Given, When, Then } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// change to sit1
Given('I am logged in to the application T27', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement T27', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I select the agreement from the search results T27', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I view the agreement details T27', async () => {
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I navigate to the schedules section T27', async () => {
    const actionsButton = await page.locator("(//button[@type='button' and contains(@class, 'ant-btn') and contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and contains(@class, 'ant-dropdown-trigger')]/span[text()='Actions'])[2]");
    await actionsButton.click();
    const schedulesItem = await page.locator("//span[text()='Schedules']");
    await schedulesItem.click();
});

Then('I verify the repayment details T27', async () => {
    const repayment = await page.locator("//td[text()='Repayment']");
    const repaymentText = await repayment.textContent();
    expect(repaymentText).toContain("Repayment");
});

Then('I verify the repayment amount T27', async () => {
    const firstCell300 = await page.locator("(//td[text()='300'])[1]");
    const firstCell300Text = await firstCell300.textContent();
    expect(firstCell300Text).toContain("300");
});

Then('I take a screenshot T27', async () => {
    await page.screenshot({ path: 'PlayWright/Pages/screenshot.png' });
});

Then('I close the browser T27', async () => {
    await page.close();
    await browser.close();
});
