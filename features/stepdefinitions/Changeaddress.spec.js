const { Given, When, Then, setDefaultTimeout } = require('@cucumber/cucumber');
const { test, expect, chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');

let page;
let browserUtil;

setDefaultTimeout(120000); // Set default timeout for the test
// changed to SIT1
Given('I am logged in to the application T19', async () => {
    const browser = await chromium.launch({ headless: false }); // Adjust launch options as needed
    page = await browser.newPage();
    page.setDefaultTimeout(120000); // Set default timeout for the page
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});

When('I click on the agreement search button T19', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I fill in the agreement number T19', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ7"); 
});

When('I click on the search button T19', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T19', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T19', async () => {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T19', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T19', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on Actions T19', async () => {
    const Actions = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await Actions.click();
});

When('I select Address History from the actions menu T19', async () => {
    const addressHistory = await page.locator("//span[contains(@class, 'ant-dropdown-menu-title-content') and text()='Address History']");
    await addressHistory.click();
});

When('I click on Edit for address details T19', async () => {
    const editButton = await page.locator('//button[@class="ant-btn css-nt5rzz ant-btn-default ant-btn-sm" and span[text()="Edit"]]');
    await editButton.click();
});

When('I update property number to 2 T19', async () => {
    const propertyNumberInput = await page.locator('//input[@placeholder="Property No." and @id="addressDetailsForm_propertyNumber"]');
    await propertyNumberInput.fill("2");
});

When('I update property name to kemal T19', async () => {
    const propertyNameInput = await page.locator('//input[@placeholder="Property Name" and @id="addressDetailsForm_propertyName"]');
    await propertyNameInput.fill("kemal");
});

When('I update street to High Lenvi Street T19', async () => {
    const streetInput = await page.locator('//input[@placeholder="Street" and @id="addressDetailsForm_street"]');
    await streetInput.fill("High Lenvi Street");
});

When('I update town to Leeds T19', async () => {
    const townInput = await page.locator('//input[@placeholder="Town" and @id="addressDetailsForm_town"]');
    await townInput.fill("Leeds");
});

When('I update postcode to LE24NY T19', async () => {
    const postcodeInput = await page.locator('//input[@placeholder="Postcode" and @id="addressDetailsForm_postcode"]');
    await postcodeInput.fill("LE24NY");
});

When('I select start date as DD-MM-YYYY T19', async () => {
    const startDateInput = await page.locator('//input[@id="addressDetailsForm_timeAtAddress_startDate" and @placeholder="DD-MM-YYYY"]');
    await startDateInput.click(); // You may need to implement date selection logic here
});

When('I submit the address details T19', async () => {
    const submitButton = await page.locator('//button[@type="submit" and contains(@class, "ant-btn") and contains(@class, "ant-btn-primary") and contains(@class, "ant-btn-sm") and .//span[text()="Submit"]]');
    await submitButton.click();
});

Then('I should see the updated street details High Lenvi Street T19', async () => {
    const lenviStreet = await page.locator('//div[@class="ant-col css-nt5rzz" and text()="High Lenvi Street"]');
    const lenviStreetText = await lenviStreet.textContent();
    expect(lenviStreetText).toContain("High Lenvi Street");
});

Then('I close the page T19', async () => {
    await page.close();
});
