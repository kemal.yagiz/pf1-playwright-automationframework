const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T52
//CHANGED TO STI1
Given('user is logged onto PF1 t52', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
    
});
Given('User searches the agreement with phonenumber t52', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    
    const telephoneNumberInput = await page.locator('input#phoneNumber');
    await telephoneNumberInput.fill("07987998877112");

    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();


    
});
Given('user then sees the phonenumber and agreement t52', async function () {
    const tdElement = await page.locator("//td[contains(@class, 'ant-table-cell') and contains(text(), '01252998877, 07987998877112')]");
    const tdText = await tdElement.textContent();
    expect(tdText).toContain("01252998877, 07987998877112");
    
await page.close();
});