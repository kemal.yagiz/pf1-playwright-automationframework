const { Given, When, Then } = require('@cucumber/cucumber');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { test, expect, chromium } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to sit1
Given('I am logged in Address', async function () {
    browser = await chromium.launch({ headless: false });
    page = await browser.newPage();
    
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});

When('I search for an agreement with agreement number X7GWAG1 Address',async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();

    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ8");
});

When('I click on the agreement from the search results Address', { timeout: 10 * 1000 },async function () {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I click on the IDV button Address', { timeout: 10 * 1000 },async function () {
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button Address', { timeout: 10 * 1000 },async function () {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on Actions Address', { timeout: 10 * 1000 },async function () {
    const Actions = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await Actions.click();
});

When('I select Address History Address', { timeout: 10 * 1000 },async function () {
    const addresshistory = await page.locator("//span[contains(@class, 'ant-dropdown-menu-title-content') and text()='Address History']");
    await addresshistory.click();
});

When('I click on Add Address Address', { timeout: 10 * 1000 },async function () {
    const addAddress = await page.locator("//span[text()='Add Address']");
    await addAddress.click();
});

When('I fill in the address details Address', { timeout: 10 * 1000 },async function () {
    const propertyNumberInput = await page.locator('//input[@placeholder="Property No." and @id="addressDetailsForm_propertyNumber"]');
    await propertyNumberInput.fill("2");

    const propertyNameInput = await page.locator('//input[@placeholder="Property Name" and @id="addressDetailsForm_propertyName"]');
    await propertyNameInput.fill("kemal");

    const streetInput = await page.locator('//input[@placeholder="Street" and @id="addressDetailsForm_street"]');
    await streetInput.fill("Lenvi Street");

    const townInput = await page.locator('//input[@placeholder="Town" and @id="addressDetailsForm_town"]');
    await townInput.fill("Leeds");

    const postcodeInput = await page.locator('//input[@placeholder="Postcode" and @id="addressDetailsForm_postcode"]');
    await postcodeInput.fill("LE24NY");

    const startDateInput = await page.locator('//input[@id="addressDetailsForm_timeAtAddress_startDate" and @placeholder="DD-MM-YYYY"]');
    await startDateInput.click();

    const dateCell = await page.locator('//div[@class="ant-picker-cell-inner" and text()="11"]');
    await dateCell.click();
});

When('I submit the address form Address',{ timeout: 10 * 1000 }, async function () {
    const submitButton = await page.locator('//button[@type="submit" and contains(@class, "ant-btn") and contains(@class, "ant-btn-primary") and contains(@class, "ant-btn-sm") and .//span[text()="Submit"]]');
    await submitButton.click();
});

Then('I should see the newly added address in the address history Address', { timeout: 10 * 1000 },async function () {
    const lenviStreet = await page.locator('//div[@class="ant-col css-nt5rzz" and text()="Lenvi Street"]');
    const lenviStreetText = await lenviStreet.textContent();
    expect(lenviStreetText).toContain("Lenvi Street");
});

Then('I remove the second address from the history Address', async function () {
    const secondRemoveButton = await page.locator('(//span[text()="Remove"])[2]');
    await secondRemoveButton.click();
    const confirm = await page.locator('//button[contains(@class, "ant-btn-primary") and contains(@class, "ant-btn-sm") and .//span[text()="Confirm"]]');
    await confirm.click();

    await page.close();
});

module.exports = { Given, When, Then };
