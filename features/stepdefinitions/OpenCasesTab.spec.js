const { Given, When, Then } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;

Given('I am logged in to the application HEPF1-T29', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.loginSIT1();
});

When('I search for the agreement HEPF1-T29', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ6");
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I select the agreement from the search results HEPF1-T29', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
    await page.waitForTimeout(1000);
});

When('I navigate to the agreement details HEPF1-T29', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

Then('I verify the Open Cases title HEPF1-T29', async () => {
    const openCasesTitle = await page.locator("//div[contains(@class, 'ant-card-head-title') and text()='Open Cases']");
    const openCasesTitleText = await openCasesTitle.textContent();
    expect(openCasesTitleText).toContain("Open Cases");
});

Then('I verify the visibility of the View button HEPF1-T29', async () => {
    const viewButton = await page.locator("//button[@class='ant-btn css-nt5rzz ant-btn-primary ant-btn-sm' and @type='button' and @style='float: right;']/span[text()='View']");
    await page.waitForTimeout(3000);
    expect(viewButton).toBeVisible();
});

Then('I close the browser HEPF1-T29', async () => {
    await browser.close();
});
