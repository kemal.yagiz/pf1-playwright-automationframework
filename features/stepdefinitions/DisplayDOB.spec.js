const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T4
//CHANGED TO STI1
Given('user is logged onto PF1', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.runTestSIT1();
});
Then('User accesses an agreement', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});
  
Then('User should see the correct DOB', async function () {
    const nameLocator = await page.locator("//span[text()='Mr Kemal Tester']");
const nameLocatorText = await nameLocator.textContent();
expect(nameLocatorText).toContain("Mr Kemal Tester");
const dobLocator = await page.locator("//span[contains(@class, 'ant-typography') and contains(@class, 'null') and contains(@class, 'fade-in') and contains(@class, 'css-nt5rzz') and text()='01-01-1993']");
const dobLocatorText = await dobLocator.textContent();
expect(dobLocatorText).toContain("01-01-1993");
const addressLocator = await page.locator("//span[contains(@class, 'ant-typography') and contains(@class, 'null') and contains(@class, 'fade-in') and contains(@class, 'css-nt5rzz') and text()='2, High St, Clifton, York, N Yorks, YO99 9ZZ']");
const addressLocatorText = await addressLocator.textContent();
expect(addressLocatorText).toContain("2, High St, Clifton, York, N Yorks, YO99 9ZZ");

await browser.close();
});