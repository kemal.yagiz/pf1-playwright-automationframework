const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application T32', async () => {
    browser = await chromium.launch({ headless: false });
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.runTestSIT1();
});

When('I click on the agreement search button T32', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I wait for 2 seconds T32', async () => {
    await page.waitForTimeout(2000);
});

When('I fill in the agreement number T32', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
});

When('I click on the search button T32', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T32', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T32', async () => {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T32', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T32', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on the Events button T32', async () => {
    const events = await page.locator("//button[@type='button' and contains(@class, 'ant-btn') and .//span[text()='Events']]");
    await events.click();
});

When('I wait for 1.5 second T32', async () => {
    await page.waitForTimeout(1000);
});

Then('I should see PF1 Access T32', async () => {
    const PF1ACCESS = await page.locator("//td[@class='ant-table-cell' and text()='PF1 Access']");
    const PF1ACCESSTEXT = await PF1ACCESS.textContent();
    expect(PF1ACCESSTEXT).toContain("PF1 Access");
});

Then('I should see New Property Valuation T32', async () => {
    const propertyvaulation = await page.locator("//td[@class='ant-table-cell' and text()='New Property Valuation']");
    const propertyvaluationtext = await propertyvaulation.textContent();
    expect(propertyvaluationtext).toContain("New Property Valuation");
});

Then('I should see Valuation Type: Purchase Price, Amount: 200000 T32', async () => {
    const purchaseprice = await page.locator("//td[@class='ant-table-cell' and text()='Valuation Type: Purchase Price, Amount: 200000']");
    const purchasepricetext = await purchaseprice.textContent();
    expect(purchasepricetext).toContain("Valuation Type: Purchase Price, Amount: 200000");
});

Then('I should see New Bank Account: 001122 12345678 T32', async () => {
    const bankdetails = await page.locator("(//td[@class='ant-table-cell' and text()='New Bank Account: 001122 12345678'])[1]");
    const bankdetailstext = await bankdetails.textContent();
    expect(bankdetailstext).toContain("New Bank Account: 001122 12345678");
});

After(async () => {
    if (page) {
        await page.close();
    }
    if (browser) {
        await browser.close();
    }
});
