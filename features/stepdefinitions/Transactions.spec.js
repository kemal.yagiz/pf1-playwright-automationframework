const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application T34', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement T34', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I select the agreement from the search results T34', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I navigate to view agreement details T34', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I take a payment T34', async () => {
    const takePaymentButton = await page.locator("//button[.//span[text()='Take Payment']]");
    await takePaymentButton.click();
    await page.reload(); // Reload the page to reset any potential state
    await takePaymentButton.click(); // Click Take Payment again after reload

    // Select payment method (e.g., Cash)
    const cashOption = await page.locator("//li[@class='ant-dropdown-menu-item ant-dropdown-menu-item-only-child' and @role='menuitem' and .//span[text()='Cash']]");
    await cashOption.click();

    // Fill payment amount, comment, and reference
    const paymentAmountInput = await page.locator("//input[@id='paymentAmount' and @class='ant-input-number-input']");
    await paymentAmountInput.fill('200');

    const commentInput = await page.locator("//input[@id='comment']");
    await commentInput.fill("Test");

    const referenceInput = await page.locator("//input[@id='reference']");
    await referenceInput.fill("Lenvi");

    await page.waitForTimeout(4000);

    // Submit the payment
    const takePaymentSubmitButton = await page.locator("//button[@type='submit' and contains(@class, 'ant-btn') and contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Take Payment']]");
    await takePaymentSubmitButton.click();
    await page.waitForTimeout(1500);
});

Then('I verify the payment transaction T34', async () => {
    // View Transactions
    const viewTransactionsButton = await page.locator("//div[@class='ant-space-item']//button[.//span[text()='View Transactions']]");
    await viewTransactionsButton.click();

    // Verify payment transaction
    const paymentTypeCell = await page.locator("//td[text()='Payment']");
    const paymentTypeText = await paymentTypeCell.textContent();
    expect(paymentTypeText).toContain("Payment");
});

Then('I close the browser T34', async () => {
    // Close the browser
    await browser.close();
});
