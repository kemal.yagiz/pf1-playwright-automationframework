const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T53
//CHANGED TO STI1
Given('user is logged onto PF1 t53', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
    
});
Given('User searches the agreement with lastname t53', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    
    const surnameInputXPath = await page.locator("input#surname");
    await surnameInputXPath.fill("Orhan123");

    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();


    
});
Given('user then sees the lastname and agreement t53', async function () {
    const tdElement = await page.locator("//td[contains(@class, 'ant-table-cell') and contains(@class, 'ant-table-column-sort') and contains(text(), 'Orhan123, Kemal')]");
const tdText = await tdElement.textContent();
expect(tdText).toContain("Orhan123, Kemal");
await page.close();
});