const { Given, When, Then } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to sit1
Given('I am logged in to the application T31', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement T31', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I select the agreement from the search results T31', async () => {
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I view the agreement details T31', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I navigate to the situation section T31', async () => {
    const actionsButton = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actionsButton.click();
    const situationItem = await page.locator("//li[.//span[text()='Situation']]");
    await situationItem.click();
});

When('I update the vulnerability status T31', async () => {
    await page.waitForTimeout(1500);
    const notVulnerable = await page.locator("//span[text()='Not Vulnerable']");
    await notVulnerable.click();
    await page.waitForTimeout(2000);
    const vulnerable = await page.locator("//span[text()='Vulnerable']");
    await vulnerable.click(); 
    await page.waitForTimeout(2000);

    const resilienceInput = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await resilienceInput.click();

    
    const vul1 = await page.locator("//div[text()='Inadequate or Erratic Income']");
    await vul1.click();

    const healthInput = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await healthInput.click();

    const vul2 = await page.locator("//div[text()='Addiction']");
    await vul2.click();

    const lifeEventInput = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await lifeEventInput.click();

    const vul3 = await page.locator("//div[text()='Bereavement']");
    await vul3.click();

    const capabilityInput = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await capabilityInput.click();

    const vul4 = await page.locator("//div[text()='Learning Difficulties']");
    await vul4.click();

    const consentToHold = await page.locator("//input[@id='vulnerabilityForm_consentToHold']");
    await consentToHold.click();
    
    const reviewDate = await page.locator("//input[@id='vulnerabilityForm_reviewDate']");
    await reviewDate.click();
    const date = await page.locator("//td[@title='2024-06-04']");
    await date.click();
    const furtherInfo = await page.locator("//textarea[@id='vulnerabilityForm_furtherInformation']");
    await furtherInfo.click();
    await furtherInfo.fill("kemal");
    const saveButton = await page.locator("(//button[.//span[text()='Save']])[4]");
    await saveButton.click();
    await page.waitForSelector('//span[@aria-label="lock"]', { state: 'visible' });
});

Then('I verify the updated vulnerability notes T31', async () => {
    await page.goto("https://he-sit1-et-pvtweb.lenvi.com/fo/home");
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
    const acknowledgeButton = await page.locator("//span[text()='Acknowledge']");
    await page.waitForSelector('//span[text()="Acknowledge"]', { state: 'visible', timeout: 60000 });
    await acknowledgeButton.click();
    const notesButton = await page.locator('//button[.//span[text()="Notes"]]');
    await notesButton.click();
    await page.waitForTimeout(1000);
    const vulnerabilityBottom = await page.locator("//td[text()='Vulnerability']");
    const vulnerabilityBottomText = await vulnerabilityBottom.textContent();
    expect(vulnerabilityBottomText).toContain("Vulnerability");
});

Then('I close the browser T31', async () => {
    await page.close();
    await browser.close();
});
