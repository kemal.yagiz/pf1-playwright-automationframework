const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');

let browser;
let page;
let browserUtil;
//changed to SIT1
Given('I am logged in to the application T35', async () => {
    browser = await chromium.launch({ headless: false });
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.runTestSIT1();
});

When('I click on the agreement search button T35', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I fill in the agreement number T35', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
});

When('I click on the search button T35', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T35', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T35', async () => {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T35', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T35', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on the Take Payment button T35', async () => {
    const takePaymentButton = await page.locator('//button[@type="button" and contains(@class, "ant-btn-primary") and contains(@class, "ant-dropdown-trigger") and span[text()="Take Payment"]]');
    await takePaymentButton.click();
});

When('I choose Cash from the payment dropdown T35', async () => {
    const cashSpan = await page.locator('//span[@class="ant-dropdown-menu-title-content" and text()="Cash"]');
    await cashSpan.click();
});

When('I fill in the payment amount as 20 T35', async () => {
    const paymentAmountInput = await page.locator('//input[@id="paymentAmount" and @role="spinbutton"]');
    await paymentAmountInput.fill("20"); // Example value, adjust as necessary
});

When('I fill in the comment as TEST KEMAL T35', async () => {
    const commentInput = await page.locator('//input[@id="comment" and @type="text"]');
    await commentInput.fill("TEST KEMAL"); // Example comment, adjust as necessary
});

When('I fill in the reference as Lenvi Kemal T35', async () => {
    const referenceInput = await page.locator('//input[@id="reference" and @type="text"]');
    await referenceInput.fill("Lenvi Kemal"); // Example reference, adjust as necessary
});

Then('I close the browser T35', async () => {
    await browser.close();
});
