const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T126
//CHANGED TO STI1
Given('​User navigates to the front office', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
    
});
Given('User selects an existing agreement', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ10"); 
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
  });
  
  Given('User accesses the Financials tab', async function () {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
  });
  
  Given('User sees Card payment and Take payment', async function () {
    const takePaymentButton = page.locator("//button[@class='ant-btn css-nt5rzz ant-btn-primary ant-btn-sm ant-dropdown-trigger']//span[text()='Take Payment']");
    await takePaymentButton.click();
  });
  
  Given('User clicks Card payment and enters amount', async function () {
    const cardSpan = page.locator("//span[@class='ant-dropdown-menu-title-content' and text()='Card']");
    await cardSpan.click();
    const paymentInput = page.locator("//input[@class='ant-input css-nt5rzz ant-input-outlined' and @placeholder='Enter payment amount']");
    await paymentInput.fill("200");
    const takePaymentButton = page.locator("//button[@class='ant-btn css-nt5rzz ant-btn-primary ant-btn-sm']/span[text()='Take Payment']");
    await takePaymentButton.click();
  });
  
  Given('User should be on the card details tab', async function () {
    const modalTitle = await page.locator("//div[contains(@class, 'ant-modal-title') and text()='Take Payment']");
    const modalTitleText = await modalTitle.textContent();
    expect(modalTitleText).toContain("Take Payment");
    await browser.close();
  });