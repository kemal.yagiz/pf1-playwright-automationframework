const { Given, When, Then } = require('@cucumber/cucumber');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { test, expect, chromium } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// Changed to STI1
Given('I am logged in', async function () {
    browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage(); 
    browserUtil = new BrowserUtil(page); 
    await browserUtil.loginSIT1(); 
});

When('I search for an agreement with agreement number', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();

    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ9");
});

When('I select the agreement from the search results', async function () {
      const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
      await searchButton.click();
      const tableRow = await page.locator('tr.ant-table-row');
      await tableRow.click();
});

When('I click on the IDV button', async function () {
    await page.waitForTimeout(1000)
  
      const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
      await idvButton.click();
});

When('I click on the View Agreement button', async function () {
    
});

Then('I should see a message indicating that acknowledgment is required for the agreement', async function () {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
      await viewAgreement.click();
      const resultTitle = await page.locator("//div[contains(@class, 'ant-result-title') and text()='YIUHAQ9 has information that must be acknowledged.']");
      const resultTitleText = await resultTitle.textContent();
      expect(resultTitleText).toContain("YIUHAQ9 has information that must be acknowledged.");
});

Then('I acknowledge the agreement', async function () {
    const acknowledge = await page.waitForSelector('//span[text()="Acknowledge"]');
    await acknowledge.click();
});

Then('I hover over the vulnerable tag', async function () {
     const vulnerable = await page.locator("//div[contains(@class, 'ant-space-item')]//span[contains(@class, 'ant-tag') and contains(@class, 'ant-tag-warning') and text()='Vulnerable']");
    await vulnerable.hover();
});

Then('I should see a message indicating vulnerability details', async function () {
      const vulnerabletext = await page.locator("//span[contains(@class, 'ant-typography') and contains(@class, 'ant-typography-warning') and text()='Resilience - Inadequate or Erratic Income, Health - Addiction, Life Event - Bereavement, Capability - Learning Difficulties']");
      const vulnerabletexttexxt = await vulnerabletext.textContent();
      expect(vulnerabletexttexxt).toContain("Resilience - Inadequate or Erratic Income, Health - Addiction, Life Event - Bereavement, Capability - Learning Difficulties")
      await page.close();
});
module.exports = { Given, When, Then }; 