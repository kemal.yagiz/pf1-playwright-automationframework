const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T82
//CHANGED TO STI1
Given('​User navigates to the front office t85', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});
Given('User selects an existing agreement t85', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ10"); 
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
  });
  
  Given('User accesses the Financials tab t85', async function () {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
  });
  
  Given('User sees cheque payment and Take payment t85', async function () {
    const takePaymentButton = page.locator("//button[@class='ant-btn css-nt5rzz ant-btn-primary ant-btn-sm ant-dropdown-trigger']//span[text()='Take Payment']");
    await takePaymentButton.click();
  });
  Given('User clicks cheque payment and enters amount t85', async function () {
    const chequePaymentButton = await page.locator("//span[contains(@class, 'ant-dropdown-menu-title-content') and text()='Cheque']");
    await chequePaymentButton.click();

    const paymentAmountInput = await page.locator("//input[@id='paymentAmount' and @role='spinbutton' and @aria-valuemin='0' and @aria-valuenow='0' and @step='0.01' and @class='ant-input-number-input']");
    await paymentAmountInput.fill("200");

    const commentInputXPath = await page.locator("//input[@id='comment' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined') and contains(@class, 'input')]");
    await commentInputXPath.fill("Test");

    const referenceInputXPath = await page.locator("//input[@id='reference' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined') and contains(@class, 'input')]");
await referenceInputXPath.fill("Lenvi");

const sortCodeInputXPath = await page.locator("//input[@id='sortCode' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined') and contains(@class, 'input')]");
await sortCodeInputXPath.fill("839146");

const accountNumberInputXPath = await page.locator("//input[@id='accountNumber' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined') and contains(@class, 'input')]");
await accountNumberInputXPath.fill("12345678");

const chequeNumberInputXPath = await page.locator("//input[@id='chequeNumber' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined') and contains(@class, 'input')]");
await chequeNumberInputXPath.fill("123");

const takePaymentButtonXPath = await page.locator("//button[@class='ant-btn css-nt5rzz ant-btn-primary ant-btn-sm']/span[text()='Take Payment']");
await takePaymentButtonXPath.click();

  });
  
  Given('User should see the success message t85', async function () {
    const chequePaymentComplete = await page.locator("//div[contains(@class, 'ant-result-title') and text()='Cheque Payment Complete']");
const chequePaymentCompleteText = await chequePaymentComplete.textContent();
expect(chequePaymentCompleteText).toContain("Cheque Payment Complete");
await browser.close();

  });