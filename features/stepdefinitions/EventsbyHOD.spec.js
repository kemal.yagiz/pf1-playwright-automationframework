const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application T42 T41 T30 T22', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement T42 T41 T30 T22', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I select the agreement from the search results T42 T41 T30 T22', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I view the agreement details T42 T41 T30 T22', async () => {
    await page.waitForTimeout(1000);
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I navigate to the Scripts section T42 T41 T30 T22', async () => {
    const scriptsButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and .//span[text()='Scripts']]");
    await scriptsButton.click();
    await page.waitForTimeout(1000);
});

When('I verify the Direct Debit Script T42 T41 T30 T22', async () => {
    const directDebitScriptButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and span[text()='Direct Debit Script']]");
    await directDebitScriptButton.click();
    const directDebitScriptTitle = await page.locator("//span[contains(@class, 'title') and text()='Direct Debit Script']");
    const directDebitScriptText = await directDebitScriptTitle.textContent();
    expect(directDebitScriptText).toContain("Direct Debit Script");
    await page.waitForTimeout(1000);
});

When('I verify the Outbound Call Script T42 T41 T30 T22', async () => {
    // const scriptsButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and .//span[text()='Scripts']]");
    // await scriptsButton.click();
    await page.waitForTimeout(1000);
    const outboundCallScriptButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and span[text()='Outbound Call Script']]");
    await outboundCallScriptButton.click();
    const outboundCallScriptTitle = await page.locator("//span[contains(@class, 'title') and text()='Outbound Call Script']");
    const outboundCallScriptText = await outboundCallScriptTitle.textContent();
    expect(outboundCallScriptText).toContain("Outbound Call Script");
    await page.waitForTimeout(1000);
});

When('I verify the Outbound Voicemail Script T42 T41 T30 T22', async () => {
    // const scriptsButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and .//span[text()='Scripts']]");
    // await scriptsButton.click();
    await page.waitForTimeout(1000);
    const outboundVoicemailScriptButton = await page.locator("//button[contains(@class, 'ant-btn-default') and contains(@class, 'ant-btn-sm') and span[text()='Outbound Voicemail Script']]");
    await outboundVoicemailScriptButton.click();
    const outboundVoicemailScriptTitle = await page.locator("//span[contains(@class, 'title') and text()='Outbound Voicemail Script']");
    const outboundVoicemailScriptText = await outboundVoicemailScriptTitle.textContent();
    expect(outboundVoicemailScriptText).toContain("Outbound Voicemail Script");
});

Then('I close the browser T42 T41 T30 T22', async () => {
    await page.close();
    await browser.close();
});
