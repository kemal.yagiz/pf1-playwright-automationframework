const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T126
//CHANGED TO STI1
Given('​User creates a new agreement t126', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});
Given('User user logs in Front Office t126', async function () {
    
});

Given('User sees the success message t126', async function () {
    const welcomeMessageLocator = await page.locator("//div[contains(@class, 'ant-result-title') and text()='Welcome to Homes England Redemptions, Kemal Yagiz!']");
const welcomeMessageText = await welcomeMessageLocator.textContent();
expect(welcomeMessageText).toContain("Welcome to Homes England Redemptions, Kemal Yagiz!");
await browser.close();
});