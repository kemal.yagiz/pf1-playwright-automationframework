const { Given, When, Then, setDefaultTimeout } = require('@cucumber/cucumber');
const { test, expect, chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');

let page;
let browserUtil;

// changed to sit1
Given('I am logged in to the application T16',async function () {
    const browser = await chromium.launch({ headless: false });
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});

When('I click on the agreement search button T16', async function () {
    const agreementSearchButton = await page.locator('button.ant-btn.agreement-search');
    await agreementSearchButton.click();
});

When('I fill in the agreement number T16', async function () {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ8");
});

When('I click on the search button T16', async function () {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T16', async function () {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T16', async function () {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T16', async function () {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T16', async function () {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on Actions T16', async function () {
    const actions = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actions.click();
});

When('I select Situation from the actions menu T16', async function () {
    const situation = await page.locator("//li[.//span[text()='Situation']]");
    await situation.click();
});

When('I wait for 1.5 seconds T16', async function () {
    await page.waitForTimeout(1500);
});

When('I click on Not Vulnerable T16', async function () {
    const notVulnerable = await page.locator("//span[text()='Not Vulnerable']");
    await notVulnerable.click();
});

When('I wait for 2 seconds T16', async function () {
    await page.waitForTimeout(2000);
});

When('I click on Vulnerable T16', async function () {
    const vulnerable = await page.locator("//span[text()='Vulnerable']");
    await vulnerable.click();
});

When('I click on the resilience input field T16', async function () {
    const resilience = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await resilience.click();
});

When('I select Inadequate or Erratic Income from the dropdown T16', async function () {
    const vul1 = await page.locator("//div[text()='Inadequate or Erratic Income']");
    await vul1.click();
});

When('I click on the health input field T16', async function () {
    const health = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await health.click();
});

When('I select Addiction from the dropdown T16', async function () {
    const vul2 = await page.locator("//div[text()='Addiction']");
    await vul2.click();
});

When('I click on the life event input field T16', async function () {
    const lifeEvent = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await lifeEvent.click();
});

When('I select Bereavement from the dropdown T16', async function () {
    const vul3 = await page.locator("//div[text()='Bereavement']");
    await vul3.click();
});

When('I click on the capability input field T16', async function () {
    const capability = await page.locator('(//span[@class="ant-select-selection-item" and @title="None"])[1]');
    await capability.click();
});

When('I select Learning Difficulties from the dropdown T16', async function () {
    const vul4 = await page.locator("//div[text()='Learning Difficulties']");
    await vul4.click();
});

When('I click on the consent to hold checkbox T16', async function () {
    const consentToHold = await page.locator("//input[@id='vulnerabilityForm_consentToHold']");
    await consentToHold.click();
});

When('I click on the review date input field T16', async function () {
    const reviewDate = await page.locator("//input[@id='vulnerabilityForm_reviewDate']");
    await reviewDate.click();
});

When('I select the date T16', async function () {
    const date = await page.locator("//td[@title='2024-06-04']");
    await date.click();
});

When('I click on the save button T16', async function () {
    const saveButton = await page.locator('(//span[text()="Save"])[4]');
    await saveButton.click();
});

Then('I should see an error message stating Missing further information T16', async function () {
    const errorMessage = await page.locator("//div[contains(@class, 'ant-form-item-explain-error') and text()='Missing further information']");
    const errorMessageText = await errorMessage.textContent();
    expect(errorMessageText).toBe("Missing further information");
});

Then('I close the page T16', async function () {
    await page.close();
});
