const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application T23', async () => {
    browser = await chromium.launch({ headless: false });
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.runTestSIT1();
});

When('I click on the agreement search button T23', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I fill in the agreement number T23', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
});

When('I click on the search button T23', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T23', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T23', async () => {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T23', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T23', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on Actions T23', async () => {
    const actionsButton = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actionsButton.click();
});

When('I select Contact Details from the menu T23', async () => {
    const contactDetailsMenuItem = await page.locator("//span[@class='ant-dropdown-menu-title-content' and text()='Contact Details']");
    await contactDetailsMenuItem.click();
});

When('I fill in the home phone number', async () => {
    const homePhoneNumberInput = await page.locator("//input[@id='ContactDetailsForm_homePhoneNumber']");
    await homePhoneNumberInput.fill("07419208820");
});

When('I fill in the mobile phone number', async () => {
    const mobilePhoneNumberInput = await page.locator("//input[@id='ContactDetailsForm_mobilePhoneNumber']");
    await mobilePhoneNumberInput.fill("07419208820");
});

When('I fill in the email address', async () => {
    const emailAddressInput = await page.locator("//input[@id='ContactDetailsForm_emailAddress']");
    await emailAddressInput.fill("kemal.yagizz@lenvi.com");
});

When('I click on the Save button', async () => {
    const saveButton = await page.locator("(//div[@class='ant-space-item']/button[@type='button' and contains(@class, 'ant-btn') and contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]/span[text()='Save'])[2]");
    await saveButton.click();
    await page.waitForTimeout(1000);
    await page.reload(); // Refresh the page after saving
});

Then('I verify the updated phone number and email address', async () => {
    const phoneNumberLocator = "//td[@class='ant-descriptions-item-content']/span/span[contains(text(), '07419208820')]";
    const phoneNumberElement = await page.locator(phoneNumberLocator);
    const phoneNumberText = await phoneNumberElement.textContent();
    expect(phoneNumberText).toContain("07419208820");

    const emailLocator = "//td[@class='ant-descriptions-item-content']/span/a[contains(@href, 'mailto') and contains(text(), '@')]";
    const emailElement = await page.locator(emailLocator);
    const emailText = await emailElement.textContent();
    expect(emailText).toContain("kemal.yagizz@lenvi.com");
    await page.close();
});


