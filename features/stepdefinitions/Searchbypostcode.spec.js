const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T54
//CHANGED TO STI1
Given('user is logged onto PF1 t54', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
    
});
Given('User searches the agreement with postcode t54', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    
    const postcodeInput = await page.locator("//input[@id='postcode' and @placeholder='Postcode' and contains(@class, 'ant-input') and contains(@class, 'css-nt5rzz') and contains(@class, 'ant-input-outlined')]");
    await postcodeInput.fill("LE18 2ED");

    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();


    
});
Given('user then sees the postcode and agreement t54', async function () {
    const tdElement = await page.locator("//td[contains(@class, 'ant-table-cell') and contains(text(), 'LE18 2ED')]");
const tdText = await tdElement.textContent();
expect(tdText).toContain("LE18 2ED");
await page.close();
});