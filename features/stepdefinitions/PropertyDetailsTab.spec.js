const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application t28', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement t28', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I select the agreement from the search results t28', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I navigate to view property details t28', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

Then('I verify the property address and amount t28', async () => {
    const addressCell = await page.locator("//td[@class='ant-descriptions-item-content']//span[contains(text(), '1, High St, Clifton, York, YO99 9ZZ')]");
    const addressCellText = await addressCell.textContent();
    expect(addressCellText).toContain("1, High St, Clifton, York, YO99 9ZZ");

    const amountCell = await page.locator("//td[@class='ant-descriptions-item-content']//span//span//span[contains(text(), '£200,000.00')]");
    const amountCellText = await amountCell.textContent();
    expect(amountCellText).toContain("£200,000.00");
});

Then('I close the browser t28', async () => {
    await page.close();
});




