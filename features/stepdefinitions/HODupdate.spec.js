const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); // Adjust path as per your folder structure
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
// changed to sit1
Given('I am logged in to the application t26 t8', async () => {
    browser = await chromium.launch({ headless: false }); // Adjust as per your setup
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page); // Adjust as per your setup
    await browserUtil.runTestSIT1();
});

When('I search for the agreement t26 t8', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I select the agreement from the search results t26 t8', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I navigate to Personal Details section t26 t8', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
    const actionsButton = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actionsButton.click();
    const personalDetailsItem = await page.locator('//li[.//span[text()="Personal Details"]]');
    await personalDetailsItem.click();
});

When('I update personal details with new information t26 t8', async () => {
    const titleDropdown = await page.locator('span.ant-select-selection-item[title="Mr"]');
    await titleDropdown.click();
    const mrOption = await page.locator('div.ant-select-item[title="Mrs"]');
    await mrOption.click();
    const surnameInput = await page.locator('//input[@id="personalDetailsForm_surname"]');
    await surnameInput.click();
    await surnameInput.fill("Orhan");
    const forenameInput = await page.locator('//input[@id="personalDetailsForm_forename"]');
    await forenameInput.fill("kemal");
    const updateButton = await page.locator('//button[@type="button" and .//span[text()="Update"]]');
    await updateButton.click();
});

Then('I verify the updated name t26 t8', async () => {
    await page.waitForTimeout(1000); // Adjust as needed
    const checkName = await page.locator("(//td[contains(@class, 'ant-descriptions-item-content')]/span/div[contains(@class, 'ant-flex')]/span)[1]");
    const checkNameText = await checkName.textContent();
    expect(checkNameText).toContain("Mr Kemal Tester"); 
});

Then('I close the browser t26 t8', async () => {
    await page.close();
});
