const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil'); 
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');
const { expect } = require('@playwright/test');

let browser;
let page;
let browserUtil;
//T55
//CHANGED TO STI1
Given('user is logged onto PF1 t55', async function () {
    const browser = await chromium.launch({ headless: false }); 
    page = await browser.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
    
});
Given('User searches the agreement with email t55', async function () {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
    
    const emailAddressInput = await page.locator("input#emailAddress.ant-input.css-nt5rzz.ant-input-outlined");
    await emailAddressInput.fill("kemal1234.yagiz@lenvi.com");

    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
    
});
Given('user then sees the email t55', async function () {
    const tdElement = await page.locator("//td[contains(@class, 'ant-table-cell') and contains(text(), 'kemal1234.yagiz@lenvi.com')]");
    const tdText = await tdElement.textContent();
expect(tdText).toContain("kemal1234.yagiz@lenvi.com");
await page.close();
});