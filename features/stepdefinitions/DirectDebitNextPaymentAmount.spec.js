const { Given, When, Then, After, setDefaultTimeout } = require('@cucumber/cucumber');
const { test, expect, chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');

let page;
let browserUtil;

setDefaultTimeout(120000); // Set default timeout for the test
// changed to SIT1
// Given I am logged in to the application T39
Given('I am logged in to the application T39', async () => {
    const browser = await chromium.launch({ headless: false }); // Adjust launch options as needed
    page = await browser.newPage();
    page.setDefaultTimeout(120000); // Set default timeout for the page
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});

// When I click on the agreement search button T39
When('I click on the agreement search button T39', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

// And I fill in the agreement number T39
When('I fill in the agreement number T39', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ7");
});

// And I click on the search button T39
When('I click on the search button T39', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

// And I click on the first agreement from the search results T39
When('I click on the first agreement from the search results T39', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

// And I wait for 1 second T39
When('I wait for 1 second T39', async () => {
    await page.waitForTimeout(1000);
});

// And I click on the IDV button T39
When('I click on the IDV button T39', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

// And I click on the View Agreement button T39
When('I click on the View Agreement button T39', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

// Then I should verify the next payment amount is £0.00 T39
Then('I should verify the next payment amount is £0.00 T39', async () => {
    const nextPaymentAmountSecond = await page.locator("(//div[contains(@class, 'ant-row')]//span[contains(@class, 'ant-typography') and text()='£0.00'])[2]");
    const nextPaymentAmountSecondText = await nextPaymentAmountSecond.textContent();
    expect(nextPaymentAmountSecondText).toContain("£0.00");
});

// And I should see the Next Payment title T39
Then('I should see the Next Payment title T39', async () => {
    const nextPaymentTitle = await page.locator("//div[@class='ant-card-head']//div[@class='ant-card-head-title' and text()='Next Payment']");
    expect(nextPaymentTitle).toBeVisible();
    await page.close();
});


