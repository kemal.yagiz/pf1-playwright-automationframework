const { Given, When, Then, After } = require('@cucumber/cucumber');
const { chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');
const { getAgreementNumber } = require('../../BrowserUtil/AgreementUtils');

let browser;
let page;
let browserUtil;
// changed to SIT1
Given('I am logged in to the application T25', async () => {
    browser = await chromium.launch({ headless: false });
    const context = await browser.newContext();
    page = await context.newPage();
    browserUtil = new BrowserUtil(page);
    await browserUtil.runTestSIT1();
});

When('I click on the agreement search button T25', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

When('I fill in the agreement number T25', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill(getAgreementNumber());
});

When('I click on the search button T25', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

When('I click on the first agreement from the search results T25', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

When('I wait for 1 second T25', async () => {
    await page.waitForTimeout(1000);
});

When('I click on the IDV button T25', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

When('I click on the View Agreement button T25', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

When('I click on Actions', async () => {
    const actionsButton = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actionsButton.click();
});

When('I select Contact Details from the menu', async () => {
    const contactDetailsMenuItem = await page.locator("//span[@class='ant-dropdown-menu-title-content' and text()='Contact Details']");
    await contactDetailsMenuItem.click();
});

When('I click on Add button', async () => {
    const addButton = await page.locator("//button[contains(@class,'ant-btn-primary')][contains(@class,'ant-btn-sm')][.//span[text()='Add']]");
    await addButton.click();
});

When('I fill in the phone number', async () => {
    const phoneNumberInput = await page.locator('input#AdditionalPhoneNumberForm_contactNumber[placeholder="Please enter a phone number"][aria-required="true"]');
    await phoneNumberInput.fill("07419208820");
});

When('I select Relative from the contact type dropdown', async () => {
    const searchInput = await page.locator('input#AdditionalPhoneNumberForm_contactTypeId[type="search"][autocomplete="off"][aria-required="true"]');
    await searchInput.click();
    await page.waitForTimeout(1000);
    const relativeOption = await page.locator('div[title="Relative"]');
    await relativeOption.click();
    await page.waitForTimeout(1000);
});

When('I fill in the description as RE', async () => {
    const descriptionInput = await page.locator('input#AdditionalPhoneNumberForm_numberInfo');
    await descriptionInput.fill("RE");
});

When('I click on the second Add button', async () => {
    const addButton2 = await page.locator('//button[@type="submit" and contains(@class, "ant-btn-primary") and contains(@class, "ant-btn-sm")]/span[text()="Add"]');
    await addButton2.click();
});

When('I save the details', async () => {
    const saveButton = await page.locator("(//div[@class='ant-space-item']/button[@type='button' and contains(@class, 'ant-btn') and contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]/span[text()='Save'])[2]");
    await saveButton.click();
});

Then('I close the browser T25', async () => {
    await browser.close();
});
