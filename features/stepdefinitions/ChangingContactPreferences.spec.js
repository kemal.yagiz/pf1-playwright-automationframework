const { Given, When, Then, After, setDefaultTimeout } = require('@cucumber/cucumber');
const { test, expect, chromium } = require('@playwright/test');
const BrowserUtil = require('../../BrowserUtil/BrowserUtil');

let page;
let browserUtil;
// chnaged to sit1
setDefaultTimeout(120000); // Set default timeout for the test
Given('I am logged in to the application T24', async () => {
    const browser = await chromium.launch({ headless: false }); // Adjust launch options as needed
    page = await browser.newPage();
    page.setDefaultTimeout(120000); // Set default timeout for the page
    browserUtil = new BrowserUtil(page);
    await browserUtil.loginSIT1();
});

// When I click on the agreement search button T24
When('I click on the agreement search button T24', async () => {
    const agreementSearchButton = await page.locator("button.ant-btn.agreement-search");
    await agreementSearchButton.click();
});

// And I fill in the agreement number T24
When('I fill in the agreement number T24', async () => {
    const agreementNumberInput = await page.locator('input[placeholder="Agreement Number"]');
    await agreementNumberInput.fill("YIUHAQ7");
});

// And I click on the search button T24
When('I click on the search button T24', async () => {
    const searchButton = await page.locator("//button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm') and .//span[text()='Search']]");
    await searchButton.click();
});

// And I click on the first agreement from the search results T24
When('I click on the first agreement from the search results T24', async () => {
    const tableRow = await page.locator('tr.ant-table-row');
    await tableRow.click();
});

// And I wait for 1 second T24
When('I wait for 1 second T24', async () => {
    await page.waitForTimeout(1000);
});

// And I click on the IDV button T24
When('I click on the IDV button T24', async () => {
    const idvButton = await page.locator("//div[@class='ant-space-item']/button[contains(@class, 'ant-btn-primary') and contains(@class, 'ant-btn-sm')]");
    await idvButton.click();
});

// And I click on the View Agreement button T24
When('I click on the View Agreement button T24', async () => {
    const viewAgreement = await page.locator("//button[contains(@class, 'ant-btn') and .//span[text()='View Agreement']]");
    await viewAgreement.click();
});

// And I click on Actions T24
When('I click on Actions T24', async () => {
    const actionsButton = await page.locator("(//button[.//span[text()='Actions']])[1]");
    await actionsButton.click();
});

// And I select Contact Details from the actions menu T24
When('I select Contact Details from the actions menu T24', async () => {
    const contactDetailsOption = await page.locator('//li[span[text()="Contact Details"]]');
    await contactDetailsOption.click();
});

// And I click on Contact at Home T24
When('I click on Contact at Home T24', async () => {
    const contactAtHomeButton = await page.locator('//button[@id="ContactDetailsForm_contactAtHome"]');
    await contactAtHomeButton.click();
});

// And I click on Contact on Mobile T24
When('I click on Contact on Mobile T24', async () => {
    const contactOnMobileButton = await page.locator('//button[@id="ContactDetailsForm_contactOnMobile"]');
    await contactOnMobileButton.click();
});

// And I click on Contact at Work T24
When('I click on Contact at Work T24', async () => {
    const contactAtWorkButton = await page.locator('//button[@id="ContactDetailsForm_contactAtWork"]');
    await contactAtWorkButton.click();
});

// And I fill in the mobile phone number with 07987998877 T24
When('I fill in the mobile phone number with 07987998877 T24', async () => {
    const mobilePhoneNumberInput = await page.locator('//input[@id="ContactDetailsForm_mobilePhoneNumber"]');
    await mobilePhoneNumberInput.fill("07987998877");
});

// And I click on Save T24
When('I click on Save T24', async () => {
    const saveButton = await page.locator('(//div[@class="ant-space-item"]//button[span[text()="Save"]])[2]');
    await saveButton.click();
});

// Then I should verify that the contact preferences are updated successfully T24


// After all scenarios, close the page
After(async () => {
    if (page) {
        await page.close();
    }
});
