const cucumberHtmlReporter = require('cucumber-html-reporter');
const path = require('path');

const options = {
    theme: 'bootstrap',
    jsonFile: path.join(__dirname, 'reports', 'cucumber-report.json'),
    output: path.join(__dirname, 'reports', 'cucumber-report.html'),
    reportSuiteAsScenarios: true,
    launchReport: true,
    metadata: {
        "App Version": "1.0.0",
        "Test Environment": "Local",
        "Browser": "Chrome",
        "Platform": "Windows 10",
        "Parallel": "Scenarios",
        "Executed": "Local"
    }
};

cucumberHtmlReporter.generate(options);
